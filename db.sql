CREATE DATABASE appointments;

create table [appointments].dbo.[user]
(
    [id]   int not null  IDENTITY (1, 1),
    [name] varchar(255),
    [birthdate] date,
    [email]     varchar(255),
    [password]  varchar(255),
    [username]  varchar(255),
    [role_id]   int not null,
    [active] bit,
    primary key ([id])
    );

create table appointments.dbo.[role]
(
    [id] int not null IDENTITY (1, 1),
    [role]    varchar(255),
    primary key ([id])
    );

create table appointments.dbo.[appointment]
(
    [id]         int not null IDENTITY (1, 1),
    [date]       datetime,
    [status_id]  int not null ,
    [doctor_id]  int not null ,
    [patient_id] int not null ,
    [note] varchar(255),
    primary key ([id])
    );

create table appointments.dbo.[status]
(
    [id] int not null IDENTITY (1, 1) ,
    [status] varchar(100),
    primary key ([id])
    );

alter table appointments.dbo.[user] add constraint [user_role_fk] foreign key ([role_id]) references [role];

alter table appointments.dbo.[appointment] add constraint [appointment_doctor_fk] foreign key ([doctor_id]) references [user];
alter table appointments.dbo.[appointment] add constraint [appointment_patient_fk] foreign key ([patient_id]) references [user];
alter table appointments.dbo.[appointment] add constraint [appointment_status_fk] foreign key ([status_id]) references [status];



INSERT INTO appointments.dbo.[status] (status) VALUES ( 'PENDING');
INSERT INTO appointments.dbo.[status] (status) VALUES ( 'COMPLETED');
INSERT INTO appointments.dbo.[status] (status) VALUES ( 'CANCELED');

INSERT INTO appointments.dbo.role (role) VALUES ('DOCTOR');
INSERT INTO appointments.dbo.role (role) VALUES ('PATIENT');

INSERT INTO appointments.dbo.[user] (name, birthdate, email, password, username, role_id,active) VALUES ( N'Doctor ONE', N'2022-06-14', N'@3', null, null,1,1);
INSERT INTO appointments.dbo.[user] (name, birthdate, email, password, username, role_id,active) VALUES ( N'Doctor two', N'2022-06-09', null, null, null,1,1);
INSERT INTO appointments.dbo.[user] (name, birthdate, email, password, username, role_id,active) VALUES (N'Patient 1', N'2022-06-09', null, null, null,2,1);
