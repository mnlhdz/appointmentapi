package com.appointment.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "appointment")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Appointment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @JoinColumn(name = "patient_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private User patient;

    @JoinColumn(name = "doctor_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private User doctor;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date")
    private Date date;

    @Column(name = "note")
    private String note;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "status_id")
    private Status status;


}
