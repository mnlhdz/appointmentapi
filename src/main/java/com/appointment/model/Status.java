package com.appointment.model;

public enum Status {
    NONE,PENDING, COMPLETED, CANCELED
}
