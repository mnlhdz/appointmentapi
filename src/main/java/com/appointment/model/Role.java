package com.appointment.model;

public enum Role {
    NONE, DOCTOR, PATIENT
}
