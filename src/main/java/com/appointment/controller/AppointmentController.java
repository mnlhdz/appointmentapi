package com.appointment.controller;

import com.appointment.model.Appointment;
import com.appointment.service.AppointmentService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/appointment")
public class AppointmentController {

    @Resource
    private AppointmentService appointmentService;

    @GetMapping("/{id}")
    public Appointment getAppointmentById(@PathVariable Integer id) {
        return appointmentService.getAppointmentById(id);
    }

    @GetMapping
    public Page<Appointment> getAppointments(Pageable pageRequest) {
        return appointmentService.getAppointments(pageRequest);
    }

    @PostMapping
    public Appointment createAppointment(@RequestBody Appointment appointment) {
        return appointmentService.createAppointment(appointment);
    }

    @PutMapping("/{id}")
    public Appointment updateAppointment(@PathVariable Integer id,@RequestBody Appointment appointment) {
        return appointmentService.updateAppointment(appointment);
    }

    @DeleteMapping("/{id}")
    public Appointment deleteAppointment(@PathVariable Integer id) {
        return appointmentService.deleteAppointment(id);
    }

    @GetMapping("/user/{id}")
    public Page<Appointment> getAppointmentByUserId(@PathVariable Integer id, Pageable pageRequest) {
        return appointmentService.getAppointmentByUserId(id, pageRequest);
    }
}
