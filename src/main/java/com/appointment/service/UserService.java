package com.appointment.service;

import com.appointment.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface UserService {
    User getUserById(Integer id);
    Page<User> getUsers(Pageable pageRequest);

    User deleteUser(Integer id);

    User createUser(User user);

    User updateUser(Integer id, User user);

    List<User> getUsersDoctors();
}
