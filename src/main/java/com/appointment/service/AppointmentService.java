package com.appointment.service;

import com.appointment.model.Appointment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface AppointmentService {
    Appointment createAppointment(Appointment appointment);
    Appointment updateAppointment(Appointment appointment);
    Appointment deleteAppointment(Integer id);
    Appointment getAppointmentById(Integer id);
    Page<Appointment> getAppointments(Pageable pageRequest);

    Page<Appointment> getAppointmentByUserId(Integer id, Pageable pageRequest);
}
