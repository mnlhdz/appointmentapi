package com.appointment.service.impl;

import com.appointment.model.Role;
import com.appointment.model.User;
import com.appointment.repository.UserRepository;
import com.appointment.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UsersServiceImpl implements UserService {

    @Resource
    private UserRepository userRepository;

    public User getUserById(Integer id) {
        return userRepository.findById(id).orElse(null);
    }

    public Page<User> getUsers(Pageable pageRequest) {
        Page<User> page = userRepository.findAll(pageRequest);
        return page;
    }

    @Override
    public User deleteUser(Integer id) {
        User user = userRepository.findById(id).orElse(null);
        if (user != null) {
            userRepository.deleteById(id);
        }
        return user;
    }

    @Override
    public User createUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public User updateUser(Integer id, User user) {
        return userRepository.save(user);
    }

    @Override
    public List<User> getUsersDoctors() {
        return userRepository.findAllByRoleAndActive(Role.DOCTOR, true);
    }


}
