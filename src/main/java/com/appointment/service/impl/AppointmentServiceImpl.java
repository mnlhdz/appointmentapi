package com.appointment.service.impl;

import com.appointment.model.Appointment;
import com.appointment.model.Role;
import com.appointment.model.User;
import com.appointment.repository.AppointmentRepository;
import com.appointment.service.AppointmentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
@Slf4j
public class AppointmentServiceImpl implements AppointmentService {

    @Resource
    private AppointmentRepository appointmentRepository;
    @Resource
    private UsersServiceImpl userService;

    @Override
    public Appointment createAppointment(Appointment appointment) {
        log.info("Appointment created: {}", appointment);
        return appointmentRepository.save(appointment);
    }

    @Override
    public Appointment updateAppointment(Appointment appointment) {
        return appointmentRepository.save(appointment);
    }

    @Override
    public Appointment deleteAppointment(Integer id) {
        Appointment appointment = appointmentRepository.findById(id).orElse(null);
        if (appointment != null) {
            appointmentRepository.deleteById(id);
        }
        return appointment;
    }

    @Override
    public Appointment getAppointmentById(Integer id) {
        return appointmentRepository.findById(id).orElse(null);
    }

    @Override
    public Page<Appointment> getAppointments(Pageable pageRequest) {
        Page<Appointment> page = appointmentRepository.findAll(pageRequest);
        return page;
    }

    @Override
    public Page<Appointment> getAppointmentByUserId(Integer id, Pageable pageRequest) {
        User user = userService.getUserById(id);
        if( user != null && user.getRole() == Role.PATIENT) {
            return appointmentRepository.findByPatientId(id, pageRequest);
        } else if (user != null && user.getRole() == Role.DOCTOR) {
            return appointmentRepository.findByDoctorId(id, pageRequest);
        }
        return null;
    }


}
