package com.appointment.repository;

import com.appointment.model.Appointment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppointmentRepository extends JpaRepository<Appointment, Integer> {
    Page<Appointment> findByPatientId(Integer id, Pageable pageRequest);

    Page<Appointment> findByDoctorId(Integer id, Pageable pageRequest);
}
