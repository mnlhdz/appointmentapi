package com.appointment.repository;

import com.appointment.model.Role;
import com.appointment.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Integer> {

    List<User> findAllByRoleAndActive(Role role, boolean active);
}
